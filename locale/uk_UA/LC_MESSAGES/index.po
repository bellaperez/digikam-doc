# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-07 00:51+0000\n"
"PO-Revision-Date: 2023-02-07 15:03+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../index.rst:1
msgid "The digiKam User Manual"
msgstr "Підручник користувача digiKam"

#: ../../index.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"help, learn"
msgstr ""
"digiKam, документація, підручник користувача, керування фотографій, "
"відкритий код, вільний, довідка, навчання, налаштування"

#: ../../index.rst:13
msgid "digiKam Manual"
msgstr "Підручник користувача digiKam"

#: ../../index.rst:15
msgid ""
"Welcome to the manual for `digiKam <https://www.digikam.org>`_, the free and "
"open source photo management program."
msgstr ""
"Вітаємо у підручник з `digiKam <https://www.digikam.org>`_, вільної програми "
"для керування фотографіями з відкритим кодом."

#: ../../index.rst:17
msgid ""
"The current digiKam version you get `here <https://www.digikam.org/download/"
">`_."
msgstr ""
"Поточну версію digiKam можна отримати `тут <https://www.digikam.org/download/"
">`_."

#: ../../index.rst:19
msgid ""
"You can download this manual as an `EPUB <https://docs.digikam.org/en/epub/"
"DigikamManual.epub>`_."
msgstr ""
"Ви можете отримати цей підручник у форматі `EPUB <https://docs.digikam.org/"
"uk_UA/epub/DigikamManual.epub>`_."

#: ../../index.rst:23
msgid "Getting Started"
msgstr "Початкові зауваження"

#: ../../index.rst:29
msgid "Do Your First Steps in digiKam Photo Management Program"
msgstr "Ваші перші кроки у програмі для керування фотографіями digiKam"

#: ../../index.rst:35
msgid ":ref:`application_intro`"
msgstr ":ref:`application_intro`"

#: ../../index.rst:39
msgid ":ref:`application_install`"
msgstr ":ref:`application_install`"

#: ../../index.rst:43
msgid ":ref:`quick_start`"
msgstr ":ref:`quick_start`"

#: ../../index.rst:47
msgid ":ref:`database_intro`"
msgstr ":ref:`database_intro`"

#: ../../index.rst:57
msgid "Supported Materials"
msgstr "Підтримувані матеріали"

#: ../../index.rst:63
msgid "File Formats and Devices Supported in digiKam"
msgstr "Формати файлів та пристрої, підтримку яких передбачено у digiKam"

#: ../../index.rst:69
msgid ":ref:`image_formats`"
msgstr ":ref:`image_formats`"

#: ../../index.rst:73
msgid ":ref:`movie_formats`"
msgstr ":ref:`movie_formats`"

#: ../../index.rst:77
msgid ":ref:`camera_devices`"
msgstr ":ref:`camera_devices`"

#: ../../index.rst:87
msgid "Main Window"
msgstr "Головне вікно"

#: ../../index.rst:93
msgid "Using the digiKam Main Window to Show Collection Contents"
msgstr "Користування основним вікном digiKam для показу вмісту збірки"

#: ../../index.rst:99
msgid ":ref:`interface_layout`"
msgstr ":ref:`interface_layout`"

#: ../../index.rst:103
msgid ":ref:`image_view`"
msgstr ":ref:`image_view`"

#: ../../index.rst:107
msgid ":ref:`albums_view`"
msgstr ":ref:`albums_view`"

#: ../../index.rst:111
msgid ":ref:`tags_view`"
msgstr ":ref:`tags_view`"

#: ../../index.rst:115
msgid ":ref:`search_view`"
msgstr ":ref:`search_view`"

#: ../../index.rst:119
msgid ":ref:`similarity_view`"
msgstr ":ref:`similarity_view`"

#: ../../index.rst:123
msgid ":ref:`mapsearch_view`"
msgstr ":ref:`mapsearch_view`"

#: ../../index.rst:127
msgid ":ref:`people_view`"
msgstr ":ref:`people_view`"

#: ../../index.rst:137
msgid "Right Sidebar"
msgstr "Права бічна панель"

#: ../../index.rst:143
msgid "Using the digiKam Right Sidebar to Play with Item Properties"
msgstr ""
"Користування правою бічною панеллю digiKam для коригування властивостей "
"записів"

#: ../../index.rst:149
msgid ":ref:`sidebar_overview`"
msgstr ":ref:`sidebar_overview`"

#: ../../index.rst:153
msgid ":ref:`properties_view`"
msgstr ":ref:`properties_view`"

#: ../../index.rst:157
msgid ":ref:`metadata_view`"
msgstr ":ref:`metadata_view`"

#: ../../index.rst:161
msgid ":ref:`colors_view`"
msgstr ":ref:`colors_view`"

#: ../../index.rst:165
msgid ":ref:`maps_view`"
msgstr ":ref:`maps_view`"

#: ../../index.rst:169
msgid ":ref:`captions_view`"
msgstr ":ref:`captions_view`"

#: ../../index.rst:173
msgid ":ref:`versions_view`"
msgstr ":ref:`versions_view`"

#: ../../index.rst:177
msgid ":ref:`filters_view`"
msgstr ":ref:`filters_view`"

#: ../../index.rst:181
msgid ":ref:`tools_view`"
msgstr ":ref:`tools_view`"

#: ../../index.rst:191
msgid "Light Table"
msgstr "Стіл з підсвічуванням"

#: ../../index.rst:197
msgid "Using the digiKam Light Table to Compare Items Side by Side"
msgstr ""
"Користування столом із підсвічуванням digiKam для паралельного порівняння "
"зображень"

#: ../../index.rst:203
msgid ":ref:`lighttable_overview`"
msgstr ":ref:`lighttable_overview`"

#: ../../index.rst:207
msgid ":ref:`lighttable_advanced`"
msgstr ":ref:`lighttable_advanced`"

#: ../../index.rst:217
msgid "Batch Queue Manager"
msgstr "Керування чергою обробки"

#: ../../index.rst:223
msgid "Using the digiKam Batch Queue Manager To Process Items in Parallel"
msgstr ""
"Користування засобом керування чергою обробки digiKam для паралельної "
"обробки записів"

#: ../../index.rst:229
msgid ":ref:`batchqueue_overview`"
msgstr ":ref:`batchqueue_overview`"

#: ../../index.rst:233
msgid ":ref:`queue_settings`"
msgstr ":ref:`queue_settings`"

#: ../../index.rst:237
msgid ":ref:`bqm_workflow`"
msgstr ":ref:`bqm_workflow`"

#: ../../index.rst:241
msgid ":ref:`base_tools`"
msgstr ":ref:`base_tools`"

#: ../../index.rst:245
msgid ":ref:`raw_converter`"
msgstr ":ref:`raw_converter`"

#: ../../index.rst:249
msgid ":ref:`dng_converter`"
msgstr ":ref:`dng_converter`"

#: ../../index.rst:253
msgid ":ref:`metadata_tools`"
msgstr ":ref:`metadata_tools`"

#: ../../index.rst:257
msgid ":ref:`custom_script`"
msgstr ":ref:`custom_script`"

#: ../../index.rst:261
msgid ":ref:`watermark_tool`"
msgstr ":ref:`watermark_tool`"

#: ../../index.rst:271
msgid "Import Tools"
msgstr "Засоби імпортування"

#: ../../index.rst:277
msgid "How to Import New Items in Your Collections With digiKam"
msgstr "Як імпортувати нові записи у вашій збірці до digiKam"

#: ../../index.rst:283
msgid ":ref:`import_overview`"
msgstr ":ref:`import_overview`"

#: ../../index.rst:287
msgid ":ref:`camera_import`"
msgstr ":ref:`camera_import`"

#: ../../index.rst:291
msgid ":ref:`advanced_import`"
msgstr ":ref:`advanced_import`"

#: ../../index.rst:295
msgid ":ref:`scanner_import`"
msgstr ":ref:`scanner_import`"

#: ../../index.rst:305
msgid "Digital Asset Management"
msgstr "Керування цифровою власністю"

#: ../../index.rst:311
msgid ""
"Learn The Basis to Handle Safety Large Digital Photography Collections In "
"Time"
msgstr ""
"Основні зауваження щодо безпеки великих цифрових збірок протягом довгого "
"періоду зберігання"

#: ../../index.rst:317
msgid ":ref:`dam_overview`"
msgstr ":ref:`dam_overview`"

#: ../../index.rst:321
msgid ":ref:`organize_find`"
msgstr ":ref:`organize_find`"

#: ../../index.rst:325
msgid ":ref:`authorship_copyright`"
msgstr ":ref:`authorship_copyright`"

#: ../../index.rst:329
msgid ":ref:`data_protection`"
msgstr ":ref:`data_protection`"

#: ../../index.rst:333
msgid ":ref:`dam_workflow`"
msgstr ":ref:`dam_workflow`"

#: ../../index.rst:343
msgid "Color Management"
msgstr "Керування кольорами"

#: ../../index.rst:349
msgid "Learn The Fundamental Rules of Color Management"
msgstr "Ознайомтеся із базовими правилами керування кольорами"

#: ../../index.rst:355
msgid ":ref:`basis_knowledge`"
msgstr ":ref:`basis_knowledge`"

#: ../../index.rst:359
msgid ":ref:`working_space`"
msgstr ":ref:`working_space`"

#: ../../index.rst:363
msgid ":ref:`monitor_profiles`"
msgstr ":ref:`monitor_profiles`"

#: ../../index.rst:367
msgid ":ref:`camera_profiles`"
msgstr ":ref:`camera_profiles`"

#: ../../index.rst:371
msgid ":ref:`printer_profiles`"
msgstr ":ref:`printer_profiles`"

#: ../../index.rst:381
msgid "Image Editor"
msgstr "Редактор зображень"

#: ../../index.rst:387
msgid "Using digiKam to Edit and Improve Your Photographs"
msgstr ""
"Користування digiKam для редагування та удосконалення ваших фотографій"

#: ../../index.rst:393
msgid ":ref:`editor_overview`"
msgstr ":ref:`editor_overview`"

#: ../../index.rst:397
msgid ":ref:`basic_operations`"
msgstr ":ref:`basic_operations`"

#: ../../index.rst:401
msgid ":ref:`workflow_tools`"
msgstr ":ref:`workflow_tools`"

#: ../../index.rst:405
msgid ":ref:`colors_tools`"
msgstr ":ref:`colors_tools`"

#: ../../index.rst:409
msgid ":ref:`enhancement_tools`"
msgstr ":ref:`enhancement_tools`"

#: ../../index.rst:413
msgid ":ref:`transform_tools`"
msgstr ":ref:`transform_tools`"

#: ../../index.rst:417
msgid ":ref:`decorate_tools`"
msgstr ":ref:`decorate_tools`"

#: ../../index.rst:421
msgid ":ref:`effects_tools`"
msgstr ":ref:`effects_tools`"

#: ../../index.rst:431
msgid "Setup Application"
msgstr "Налаштовування програми"

#: ../../index.rst:437
msgid "Using the digiKam Configuration Panel to Customize Application"
msgstr ""
"Користування панеллю налаштовування digiKam для налаштовування програми"

#: ../../index.rst:443
msgid ":ref:`config_overview`"
msgstr ":ref:`config_overview`"

#: ../../index.rst:447
msgid ":ref:`database_settings`"
msgstr ":ref:`database_settings`"

#: ../../index.rst:451
msgid ":ref:`collections_settings`"
msgstr ":ref:`collections_settings`"

#: ../../index.rst:455
msgid ":ref:`views_settings`"
msgstr ":ref:`views_settings`"

#: ../../index.rst:459
msgid ":ref:`tooltip_settings`"
msgstr ":ref:`tooltip_settings`"

#: ../../index.rst:463
msgid ":ref:`metadata_settings`"
msgstr ":ref:`metadata_settings`"

#: ../../index.rst:467
msgid ":ref:`templates_settings`"
msgstr ":ref:`templates_settings`"

#: ../../index.rst:471
msgid ":ref:`editor_settings`"
msgstr ":ref:`editor_settings`"

#: ../../index.rst:475
msgid ":ref:`cm_settings`"
msgstr ":ref:`cm_settings`"

#: ../../index.rst:479
msgid ":ref:`lighttable_settings`"
msgstr ":ref:`lighttable_settings`"

#: ../../index.rst:483
msgid ":ref:`imgqsort_settings`"
msgstr ":ref:`imgqsort_settings`"

#: ../../index.rst:487
msgid ":ref:`camera_settings`"
msgstr ":ref:`camera_settings`"

#: ../../index.rst:491
msgid ":ref:`plugins_settings`"
msgstr ":ref:`plugins_settings`"

#: ../../index.rst:495
msgid ":ref:`miscs_settings`"
msgstr ":ref:`miscs_settings`"

#: ../../index.rst:499
msgid ":ref:`theme_settings`"
msgstr ":ref:`theme_settings`"

#: ../../index.rst:503
msgid ":ref:`shortcuts_settings`"
msgstr ":ref:`shortcuts_settings`"

#: ../../index.rst:513
msgid "Geolocation Editor"
msgstr "Редактор геопозиціонування"

#: ../../index.rst:519
msgid "Edit Geolocation Information to Search And Visualize Items on a Map"
msgstr ""
"Редагування даних геопозиціювання для пошуку і візуалізації записів на карті"

#: ../../index.rst:525
msgid ":ref:`geoeditor_overview`"
msgstr ":ref:`geoeditor_overview`"

#: ../../index.rst:529
msgid ":ref:`geoeditor_map`"
msgstr ":ref:`geoeditor_map`"

#: ../../index.rst:533
msgid ":ref:`geoeditor_coordinates`"
msgstr ":ref:`geoeditor_coordinates`"

#: ../../index.rst:537
msgid ":ref:`geoeditor_reverse`"
msgstr ":ref:`geoeditor_reverse`"

#: ../../index.rst:541
msgid ":ref:`geoeditor_search`"
msgstr ":ref:`geoeditor_search`"

#: ../../index.rst:545
msgid ":ref:`geoeditor_kmlexport`"
msgstr ":ref:`geoeditor_kmlexport`"

#: ../../index.rst:555
msgid "Maintenance Tools"
msgstr "Засоби супроводу збірки"

#: ../../index.rst:561
msgid "Maintaining and Optimize Data From Your Collections"
msgstr "Супровід та оптимізація даних у вашій збірці"

#: ../../index.rst:567
msgid ":ref:`maintenance_overview`"
msgstr ":ref:`maintenance_overview`"

#: ../../index.rst:571
msgid ":ref:`maintenance_common`"
msgstr ":ref:`maintenance_common`"

#: ../../index.rst:575
msgid ":ref:`maintenance_newitems`"
msgstr ":ref:`maintenance_newitems`"

#: ../../index.rst:579
msgid ":ref:`maintenance_database`"
msgstr ":ref:`maintenance_database`"

#: ../../index.rst:583
msgid ":ref:`maintenance_thumbnails`"
msgstr ":ref:`maintenance_thumbnails`"

#: ../../index.rst:587
msgid ":ref:`maintenance_fingerprints`"
msgstr ":ref:`maintenance_fingerprints`"

#: ../../index.rst:591
msgid ":ref:`maintenance_duplicates`"
msgstr ":ref:`maintenance_duplicates`"

#: ../../index.rst:595
msgid ":ref:`maintenance_faces`"
msgstr ":ref:`maintenance_faces`"

#: ../../index.rst:599
msgid ":ref:`maintenance_quality`"
msgstr ":ref:`maintenance_quality`"

#: ../../index.rst:603
msgid ":ref:`maintenance_metadata`"
msgstr ":ref:`maintenance_metadata`"

#: ../../index.rst:613
msgid "Post Processing"
msgstr "Остаточна обробка"

#: ../../index.rst:619
msgid "Post Process Items From Your Collections"
msgstr "Остаточна обробка записів з вашої збірки"

#: ../../index.rst:625
msgid ":ref:`metadata_editor`"
msgstr ":ref:`metadata_editor`"

#: ../../index.rst:629
msgid ":ref:`html_gallery`"
msgstr ":ref:`html_gallery`"

#: ../../index.rst:633
msgid ":ref:`media_server`"
msgstr ":ref:`media_server`"

#: ../../index.rst:637
msgid ":ref:`send_images`"
msgstr ":ref:`send_images`"

#: ../../index.rst:641
msgid ":ref:`print_creator`"
msgstr ":ref:`print_creator`"

#: ../../index.rst:645
msgid ":ref:`time_adjust`"
msgstr ":ref:`time_adjust`"

#: ../../index.rst:655
msgid "Export Tools"
msgstr "Засоби експортування"

#: ../../index.rst:661
msgid "Export Items From Your Collections To External Media"
msgstr "Експортування записів вашої збірки на зовнішній носій даних"

#: ../../index.rst:667
msgid ":ref:`flickr_export`"
msgstr ":ref:`flickr_export`"

#: ../../index.rst:677
msgid "Slideshow Tools"
msgstr "Засоби показу слайдів"

#: ../../index.rst:683
msgid "How to Use Tools to Present Your Items"
msgstr "Як користуватися інструментами для представлення записів збірки"

#: ../../index.rst:689
msgid ":ref:`slide_tool`"
msgstr ":ref:`slide_tool`"

#: ../../index.rst:693
msgid ":ref:`presentation_tool`"
msgstr ":ref:`presentation_tool`"

#: ../../index.rst:697
msgid ":ref:`opengl_viewer`"
msgstr ":ref:`opengl_viewer`"

#: ../../index.rst:707
msgid "Showfoto"
msgstr "Showfoto"

#: ../../index.rst:713
msgid "Showfoto is a Stand Alone Version of the digiKam Image Editor"
msgstr "Showfoto — окрема версія редактора зображень digiKam"

#: ../../index.rst:719
msgid ":ref:`showfoto_overview`"
msgstr ":ref:`showfoto_overview`"

#: ../../index.rst:723
msgid ":ref:`showfoto_leftsidebar`"
msgstr ":ref:`showfoto_leftsidebar`"

#: ../../index.rst:727
msgid ":ref:`showfoto_setup`"
msgstr ":ref:`showfoto_setup`"

#: ../../index.rst:731
msgid ":ref:`showfoto_tools`"
msgstr ":ref:`showfoto_tools`"

#: ../../index.rst:741
msgid "Menu Descriptions"
msgstr "Опис меню програми"

#: ../../index.rst:747
msgid "Learn Details About All digiKam Menus"
msgstr "Докладніше про усі меню digiKam"

#: ../../index.rst:753
msgid ":ref:`menu_mainwindow`"
msgstr ":ref:`menu_mainwindow`"

#: ../../index.rst:757
msgid ":ref:`menu_imageeditor`"
msgstr ":ref:`menu_imageeditor`"

#: ../../index.rst:761
msgid ":ref:`menu_batchqueue`"
msgstr ":ref:`menu_batchqueue`"

#: ../../index.rst:765
msgid ":ref:`menu_lighttable`"
msgstr ":ref:`menu_lighttable`"

#: ../../index.rst:769
msgid ":ref:`menu_importtool`"
msgstr ":ref:`menu_importtool`"

#: ../../index.rst:773
msgid ":ref:`menu_showfoto`"
msgstr ":ref:`menu_showfoto`"

#: ../../index.rst:783
msgid "Credits and License"
msgstr "Подяки і ліцензія"

#: ../../index.rst:789
msgid "Copyrights and Notices About This Document"
msgstr "Авторські права та зауваження щодо цього документа"

#: ../../index.rst:795
msgid ":ref:`credits_license`"
msgstr ":ref:`credits_license`"

#: ../../index.rst:805
msgid "Get Involved"
msgstr "Участь у команді"

#: ../../index.rst:811
msgid "Contribute to digiKam project"
msgstr "Зробити внесок у проєкт digiKam"

#: ../../index.rst:817
#| msgid ":ref:`image_view`"
msgid ":ref:`get_involved`"
msgstr ":ref:`get_involved`"

#~| msgid ":ref:`colors_view`"
#~ msgid ":ref:`srgb_colorspace`"
#~ msgstr ":ref:`srgb_colorspace`"

#~ msgid "Tag Manager"
#~ msgstr "Керування мітками"

#~ msgid ":ref:`tagmanager_overview`"
#~ msgstr ":ref:`tagmanager_overview`"

#~ msgid ":ref:`share_dlna`"
#~ msgstr ":ref:`share_dlna`"
