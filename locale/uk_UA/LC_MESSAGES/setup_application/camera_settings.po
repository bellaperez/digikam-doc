# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-17 00:50+0000\n"
"PO-Revision-Date: 2023-01-11 00:00+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../setup_application/camera_settings.rst:1
msgid "digiKam Camera Settings"
msgstr "Параметри фотоапарата digiKam"

#: ../../setup_application/camera_settings.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, camera, configuration, setup, gphoto2, usb, mass, storage"
msgstr ""
"digiKam, документація, підручник користувача, керування фотографій, "
"відкритий код, вільний, навчання, простий, фотоапарат, налаштування, "
"налаштовування, gphoto2, usb, зберігання, дані, носій"

#: ../../setup_application/camera_settings.rst:14
msgid "Camera Settings"
msgstr "Параметри фотоапарата"

#: ../../setup_application/camera_settings.rst:16
msgid "Contents"
msgstr "Зміст"

#: ../../setup_application/camera_settings.rst:19
msgid "Setup a Device"
msgstr "Налаштовування пристрою"

#: ../../setup_application/camera_settings.rst:21
msgid ""
"The camera settings shows on the left of the list of the currently supported "
"cameras. On the right at the top there is an auto-detect button, which tries "
"to identify the camera connected to your computer (make sure that the camera "
"is connected properly to the computer and turned on in the image display "
"mode). Below this are the port settings, the currently supported are "
"**Serial**, **USB**, **Network**, and **Mass Storage**."
msgstr ""
"Параметри фотоапарата буде показано у лівій частині поточного списку "
"підтримуваних фотоапаратів. Вгорі праворуч розташовано кнопку автоматичного "
"визначення, після натискання якої програма спробує визначити параметри "
"фотоапарата з’єднаного з комп’ютером (не забудьте належним чином з’єднати "
"фотоапарат з комп’ютером та увімкнути його у режимі показу знімків). Нижче "
"розташовано поля визначення параметрів порту. У поточній версії програми "
"передбачено підтримку роботи з **послідовним портом**, **портом USB**, "
"**мережею** та **носіями даних**."

#: ../../setup_application/camera_settings.rst:27
msgid "The digiKam Camera Configuration Panel with the Gphoto View"
msgstr ""
"Панель перегляду параметрів Gphoto панелі налаштувань фотоапарата digiKam"

#: ../../setup_application/camera_settings.rst:29
msgid ""
"Clicking on a camera in the list from the left will display the supported "
"ports which you can then select. If there is only one supported port it will "
"be automatically selected. At the bottom on the right there is a box for "
"setting the exact path in case of a serial port. Please note that USB "
"interface does not need any paths to be set. If you cannot find your camera "
"on the list, you can try to use a generic **Mass Storage** device selecting "
"Mounted Camera item in the list."
msgstr ""
"Після натискання пункту фотоапарата у списку, розташованому ліворуч, "
"програма покаже список підтримуваних портів, один з яких ви зможете вибрати. "
"Якщо можливим є лише один з портів, його буде позначено автоматично. Внизу "
"праворуч буде розташовано поле для визначення точного шляху до файла "
"пристрою у разі послідовного порту. Будь ласка, зауважте, що для інтерфейсів "
"USB ніяких шляхів вказувати не потрібно. Якщо вам не вдасться знайти пункт "
"вашого фотоапарата у списку, ви можете скористатися пунктом типового "
"пристрою зберігання даних вибором пункту «Змонтований фотоапарат» зі списку."

#: ../../setup_application/camera_settings.rst:31
msgid ""
"At the very bottom is where you set the path for a USB or FireWire "
"(IEEE-1394 or i-link) Mass Storage camera. This box becomes active once you "
"select USB or FireWire Mass Storage camera in the camera list. You need to "
"enter here the path where you mount the camera, usually :file:`/mnt/camera` "
"or :file:`/mnt/removable`."
msgstr ""
"Найнижче розташовано поле, у якому ви можете вказати шлях до сховища даних "
"фотоапарата на порті USB або FireWire (IEEE-1394 або i-link). Це поле буде "
"задіяно після вибору вами пунктів фотоапаратів з носіями даних USB або "
"FireWire у списку фотоапаратів. У це поле слід ввести шлях до теки, куди "
"було змонтовано ваш фотоапарат, наприклад, :file:`/mnt/camera` або :file:`/"
"mnt/removable`."

#: ../../setup_application/camera_settings.rst:37
msgid "Dialog to Add a Camera Device Using Gphoto Drivers"
msgstr "Вікно додавання пристрою фотоапарата з використанням драйверів Gphoto"

#: ../../setup_application/camera_settings.rst:39
msgid ""
"To be able to use your digital camera with digiKam, connect the camera to "
"your computer, switch the camera to the image display mode and turn it on."
msgstr ""
"Щоб мати змогу користуватися вашим цифровим фотоапаратом за допомогою "
"digiKam, з’єднайте фотоапарат з вашим комп’ютером, перемкніть фотоапарат у "
"режим показу зображень і увімкніть його."

#: ../../setup_application/camera_settings.rst:41
msgid ""
"Try and see if digiKam can auto-detect the camera; if not, you can set the "
"camera model and port manually. Once you have the camera setup, go to the "
"**Cameras** menu in the main interface and you will see the camera listed in "
"the menu."
msgstr ""
"Дайте змогу digiKam автоматично визначити ваш фотоапарат. Якщо автоматичне "
"визначення виконати не вдасться, ви можете вказати модель фотоапарата і порт "
"вручну. Після налаштування фотоапарата відкрийте меню **Фотоапарати** у "
"головному вікні програми: у списку має з’явитися пункт вашого фотоапарата."

#: ../../setup_application/camera_settings.rst:45
msgid ""
"You can choose any title you like for the camera in the setup and this title "
"will be used in the main window **Cameras** menu. If you have more than one "
"camera, you can add them through this setup interface."
msgstr ""
"У діалоговому вікні налаштування фотоапарата ви можете вибрати для "
"фотоапарата будь-яку назву. Цю назву буде використано у меню **Фотоапарати** "
"головного вікна програми. Якщо ви є власником чи власницею декількох "
"фотоапаратів, ви можете додати їх записи за допомогою цього інтерфейсу "
"налаштування."

#: ../../setup_application/camera_settings.rst:50
msgid "Customize the Behavior"
msgstr "Налаштовування поведінки"

#: ../../setup_application/camera_settings.rst:52
msgid ""
"With the **Behavior** section, you can customize some main rules with your "
"camera as how to deep in file to handle metadata, render high-quality "
"thumbnails, use a default target album to download items, and to deal with "
"already existing file-names while downloading."
msgstr ""
"За допомогою розділу **Поведінка** ви можете налаштувати певні основні "
"правила для фотоапарата щодо записів метаданих, обробки високоякісних "
"мініатюр, використання типового альбому для отримання зображень та обробки "
"наявних назв файлів під час отримання даних."

#: ../../setup_application/camera_settings.rst:56
msgid ""
"Some options can slow-down the connection stage to the camera. Application "
"will populate icon-view with in-deep metadata while scanning items from the "
"device."
msgstr ""
"Використання деяких параметрів може уповільнити крок встановлення з'єднання "
"із фотоапаратом. Програма змушена буде заповнювати панель перегляду "
"піктограм глибоко вбудованими метаданими при скануванні записів на пристрої."

#: ../../setup_application/camera_settings.rst:62
msgid "The digiKam Camera Configuration Panel with the Behavior View"
msgstr "Панель перегляду поведінки панелі налаштувань фотоапарата digiKam"

#: ../../setup_application/camera_settings.rst:67
msgid "Running Filters at Download"
msgstr "Запуск фільтрів при отриманні даних"

#: ../../setup_application/camera_settings.rst:69
msgid ""
"With the **Import Filter** section, you can customize the list of type mime "
"filters, and list the file patterns to ignore while downloading."
msgstr ""
"За допомогою розділу **Фільтр імпортування** ви можете налаштувати список "
"фільтрів за типом MIME і переглянути список взірців файлів, які слід "
"ігнорувати під час отримання даних."

#: ../../setup_application/camera_settings.rst:75
msgid "The digiKam Camera Configuration Panel with the Import Filters View"
msgstr ""
"Панель перегляду фільтрів імпортування панелі налаштувань фотоапарата digiKam"

#: ../../setup_application/camera_settings.rst:80
msgid "Customize Import Interface"
msgstr "Налаштовування інтерфейсу імпортування"

#: ../../setup_application/camera_settings.rst:82
msgid ""
"With the **Import Window** section, you can customize options for icon-view "
"information, the preview behavior, and the full-screen elements to show."
msgstr ""
"За допомогою розділу **Вікно імпортування** ви можете налаштувати параметри "
"показаних відомостей, поведінку при попередньому перегляді та перелік "
"показаних у повноекранному режимі елементів."

#: ../../setup_application/camera_settings.rst:88
msgid "The digiKam Camera Configuration Panel with the Import Window View"
msgstr ""
"Панель перегляду вікна імпортування панелі налаштувань фотоапарата digiKam"

#~ msgid "..note ::"
#~ msgstr "..note ::"
