# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-27 00:51+0000\n"
"PO-Revision-Date: 2023-01-26 09:16+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../showfoto_editor.rst:1
msgid "The Showfoto Stand Alone Image Editor"
msgstr "Окремий редактор зображень Showfoto"

#: ../../showfoto_editor.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"help, learn, image editor, showfoto"
msgstr ""
"digiKam, документація, підручник користувача, керування фотографій, "
"відкритий код, вільний, довідка, навчання, зображення, редактор, showfoto, "
"шоуфото"

#: ../../showfoto_editor.rst:15
msgid "Showfoto"
msgstr "Showfoto"

#: ../../showfoto_editor.rst:19
msgid "This section explain how to use the Showfoto image editor."
msgstr ""
"У цьому розділі наведено пояснення щодо користування редактором зображень "
"Showfoto."

#: ../../showfoto_editor.rst:21
msgid "Contents:"
msgstr "Зміст:"
