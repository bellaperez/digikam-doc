# Translation of docs_digikam_org_export_tools___flickr_export.po to Catalan
# Copyright (C) 2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">terms of the GNU Free Documentation License 1.2</a> unless stated otherwise
# This file is distributed under the same license as the digiKam Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: digikam-doc\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-06 00:52+0000\n"
"PO-Revision-Date: 2023-02-06 08:39+0100\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 3.1.1\n"

#: ../../export_tools/flickr_export.rst:1
msgid "digiKam Export to Flickr Web-Service"
msgstr "Exportar cap al servei web de Flickr en el digiKam"

#: ../../export_tools/flickr_export.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, flickr, export"
msgstr ""
"digiKam, documentació, manual d'usuari, gestió de les fotografies, codi "
"obert, lliure, ajuda, aprenentatge, fàcil, flickr, exporta"

#: ../../export_tools/flickr_export.rst:14
msgid "Export To Flickr"
msgstr "Exportar cap al Flickr"

#: ../../export_tools/flickr_export.rst:16
msgid "Contents"
msgstr "Contingut"

#: ../../export_tools/flickr_export.rst:18
msgid "This tool allows the user to upload photos to the Flickr web service."
msgstr ""
"Aquesta eina permet a l'usuari publicar fotografies en el servei web de "
"Flickr."

#: ../../export_tools/flickr_export.rst:20
msgid ""
"Flickr is an online photo management application. Flickr is a way to get "
"your photos to the people who matter to you. With Flickr you can show off "
"your favorite photos to the world, blog the photos you take with a "
"smartphone, and securely and privately show photos to your friends and "
"family around the world."
msgstr ""
"El Flickr és una aplicació per a la gestió fotogràfica en línia. El Flickr "
"és una manera d'oferir les vostres fotografies a les persones que us "
"importen. Amb el Flickr podreu mostrar les vostres fotografies preferides a "
"tothom, un blog de les fotografies que preneu amb un telèfon intel·ligent, i "
"de forma segura i privada mostrar-les als amics i familiars a tot el món."

#: ../../export_tools/flickr_export.rst:22
msgid ""
"The tool can be used to upload an image collection from your computer to the "
"remote flickr server using the Internet."
msgstr ""
"L'eina es pot utilitzar per a publicar una col·lecció d'imatges des del "
"vostre ordinador cap al servidor remot de Flickr mitjançant Internet."

#: ../../export_tools/flickr_export.rst:24
msgid ""
"When accessing the tool for the first time you are taken through the process "
"of obtaining a token which is used for authentication purposes. The "
"following dialog will popup and a browser window will be launched you will "
"log in to Flickr:"
msgstr ""
"En accedir a l'eina per primera vegada passareu pel procés d'obtenció d'una "
"fitxa que s'utilitzarà amb la finalitat d'autenticar-se. Apareixerà el "
"diàleg següent i es llançarà una finestra del navegador que iniciarà la "
"sessió al Flickr:"

#: ../../export_tools/flickr_export.rst:30
msgid "The Flickr Export Login Dialog"
msgstr "El diàleg d'inici de sessió per a l'exportació de Flickr"

#: ../../export_tools/flickr_export.rst:32
msgid ""
"After successful signup digiKam will be allowed to send photos to the flickr "
"website. You will be presented with the following page on successful signup:"
msgstr ""
"Després d'un registre amb èxit, el digiKam podrà enviar fotografies al lloc "
"web de Flickr. en un registre amb èxit se us presentarà la pàgina següent:"

#: ../../export_tools/flickr_export.rst:38
msgid "The Flickr Export Authorize Dialog"
msgstr "El diàleg d'autorització per a l'exportació de Flickr"

#: ../../export_tools/flickr_export.rst:40
msgid ""
"Then, simply authorize application and close the web browser. Return to the "
"host application dialog, you will see the interface used to upload photos to "
"Flickr, described in the next section."
msgstr ""
"A continuació, senzillament autoritzeu l'aplicació i tanqueu el navegador "
"web. Torneu al diàleg de l'aplicació amfitriona, veureu la interfície que "
"s'utilitza per a publicar fotografies al Flickr, es descriu a la secció "
"següent."

#: ../../export_tools/flickr_export.rst:42
msgid ""
"When the tool is invoked for second time or later you are greeted with the "
"following dialog, which shows the current account already used previously. "
"Just select one from the list to be connected."
msgstr ""
"Quan s'invoca l'eina per segona vegada o després, saluda amb el diàleg "
"següent, el qual mostra el compte actual ja utilitzat anteriorment. Només "
"cal que en seleccioneu un des de la llista per a connectar-hi."

#: ../../export_tools/flickr_export.rst:48
msgid "The Flickr Export Dialog to Select the Previously Registered Account"
msgstr ""
"El diàleg d'exportació de Flickr per a seleccionar el compte registrat "
"anteriorment"

#: ../../export_tools/flickr_export.rst:50
msgid "The Main upload dialog is shown below:"
msgstr "A continuació es mostra el diàleg principal de publicació:"

#: ../../export_tools/flickr_export.rst:56
msgid "The Flickr Export Tool Dialog"
msgstr "El diàleg de l'eina d'exportació de Flickr"

#: ../../export_tools/flickr_export.rst:58
msgid ""
"By default, the tool proposes to export the currently selected items from "
"the icon-view. The **+** Photos button can be used to append more items on "
"the list."
msgstr ""
"De manera predeterminada, l'eina proposa exportar els elements seleccionats "
"des de la vista d'icones. El botó **+ Fotografies** es pot utilitzar per a "
"afegir més elements a la llista."

#: ../../export_tools/flickr_export.rst:60
msgid ""
"The **Tag options** section can be used to apply digiKam keywords to the "
"flickr upload. More keywords can be assigned with a series of words (comma "
"separated)."
msgstr ""
"La secció **Opcions de l'etiqueta** es pot utilitzar per a aplicar paraules "
"clau del digiKam a la publicació al Flickr. Es poden assignar més paraules "
"clau amb una sèrie de paraules (separades per comes)."

#: ../../export_tools/flickr_export.rst:62
msgid ""
"The **Public**, **Friends**, and **Family** check-boxes can be turned on to "
"assign appropriate access permissions to the images you upload. By default "
"the images uploaded are private."
msgstr ""
"Les caselles de selecció **Pública**, **Amics** i **Família** es poden "
"marcar per a assignar permisos d'accés adequats a les imatges que publiqueu. "
"De manera predeterminada, les imatges publicades seran privades."

#: ../../export_tools/flickr_export.rst:64
msgid ""
"If the **Resize photos before uploading** option is selected, the photos "
"will be resized before transferring to Flickr. The values will be read from "
"the **Maximum Dimension** combo-box, which can be used to adjust the maximum "
"height. The width calculation will be done so as to have the aspect ratio "
"conserved."
msgstr ""
"Si se selecciona l'opció **Redimensiona les fotografies abans de pujar-"
"les**, les fotografies es redimensionaran abans de transferir-les al Flickr. "
"Els valors es llegiran a partir del quadre combinat **Dimensió màxima:**, el "
"qual es pot utilitzar per a ajustar l'alçada màxima. Es farà el càlcul de "
"l'amplada per tal de conservar la relació d'aspecte."

#: ../../export_tools/flickr_export.rst:70
msgid "The Flickr Export Uploading in Progress"
msgstr "L'exportació de Flickr pujant elements està en curs"

#: ../../export_tools/flickr_export.rst:72
msgid ""
"Press **Start Uploading** button to transfer items. You can click Cancel "
"button to abort the uploading of photos."
msgstr ""
"Premeu **Inici de la pujada** per a transferir els elements. Podeu fer clic "
"en el botó Cancel·la per a interrompre la publicació de les fotografies."

#: ../../export_tools/flickr_export.rst:74
msgid ""
"Finally, you can view the uploaded photos by visiting the Flickr website."
msgstr ""
"Finalment, podreu veure les fotografies publicades visitant el lloc web de "
"Flickr."

#: ../../export_tools/flickr_export.rst:80
msgid "The Flickr Online Account Displaying the Uploaded Contents"
msgstr "El compte en línia de Flickr mostrant el contingut que s'ha pujat"
