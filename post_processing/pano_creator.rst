.. meta::
   :description: The digiKam Panorama Creator
   :keywords: digiKam, documentation, user manual, photo management, open source, free, learn, easy, panorama, assembly, stitch

.. metadata-placeholder

   :authors: - digiKam Team

   :license: see Credits and License page for details (https://docs.digikam.org/en/credits_license.html)

.. _pano_creator:

Panorama Creator
================

.. contents::

One of the most exciting tools available in digiKam and Showfoto is the Panorama tool. Which is simple and quite effective.

Select the images that you want to stitch and go to :menuselection:`Tools -> Create Panorama` a wizard dialog will open up as below.

.. figure:: images/pano_creator_01.webp
    :alt:
    :align: center

    The First Page of the Panorama Creator Wizard

This plugin is based on the `Hugin command line tools <https://hugin.sourceforge.io/>`_ to assemble the items. If all software components are found of your system, just select any output format that you prefer to render the panorama and click on **Next** button. And this window appears:

.. figure:: images/pano_creator_02.webp
    :alt:
    :align: center

    The Second Page of the Panorama Creator Wizard

Arrange the images in correct order if they are not, use the up and down arrows, click the icon with the red cross to remove any images and click on **Next** button.

.. figure:: images/pano_creator_03.webp
    :alt:
    :align: center

    The Third Page of the Panorama Creator Wizard

If you want the tool **Detect moving skies** then click the option and press **Next** button to proceed.

.. figure:: images/pano_creator_04.webp
    :alt:
    :align: center

    The 4th Page of the Panorama Creator Wizard

Post processing begins and when it is done click on the **Next** button.

.. figure:: images/pano_creator_05.webp
    :alt:
    :align: center

    The 5th Page of the Panorama Creator Wizard

In this window the images are being processed! When done click on **Next** button.

.. figure:: images/pano_creator_06.webp
    :alt:
    :align: center

    The Last Page of the Panorama Creator Wizard

Panorama stitching is now done. If you want you can now save your project. The same project files can be used in Hugin. Click on **Next** button and you will see the output in the same **Album** as your original pictures.
